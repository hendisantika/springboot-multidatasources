package com.hendisantika.springbootmultidatasources.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-multidatasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/01/20
 * Time: 07.04
 */
@Slf4j
@ControllerAdvice
public class ExceptionHandlerControllerAdvice {
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public ExceptionResponse handleResourceNotFound(final ResourceNotFoundException exception,
                                                    final HttpServletRequest request) {
        log.error(exception.getMessage());

        ExceptionResponse error = new ExceptionResponse();
        error.setMessage(exception.getMessage());
        error.setUrl(request.getRequestURI());
        error.setStatus(HttpStatus.NOT_FOUND.value());

        return error;
    }
}
