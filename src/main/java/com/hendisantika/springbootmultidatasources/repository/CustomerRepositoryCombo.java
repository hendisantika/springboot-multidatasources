package com.hendisantika.springbootmultidatasources.repository;

import com.hendisantika.springbootmultidatasources.repository.read.CustomerReadRepository;
import com.hendisantika.springbootmultidatasources.repository.write.CustomerWriteRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-multidatasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/01/20
 * Time: 07.54
 */
public interface CustomerRepositoryCombo extends CustomerReadRepository, CustomerWriteRepository {
}
