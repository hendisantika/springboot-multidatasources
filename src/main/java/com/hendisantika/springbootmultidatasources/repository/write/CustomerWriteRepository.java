package com.hendisantika.springbootmultidatasources.repository.write;

import com.hendisantika.springbootmultidatasources.model.Customer;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-multidatasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/01/20
 * Time: 07.55
 */
public interface CustomerWriteRepository extends CrudRepository<Customer, Long> {
}