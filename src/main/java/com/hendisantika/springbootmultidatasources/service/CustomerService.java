package com.hendisantika.springbootmultidatasources.service;

import com.hendisantika.springbootmultidatasources.model.Customer;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-multidatasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/01/20
 * Time: 07.59
 */
public interface CustomerService {

    Optional<Customer> getCustomer(Long id);

    Customer createCustomer(Customer customer);

    Customer updateCustomer(Customer customer);
}
